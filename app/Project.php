<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    protected $fillable = ['name', 'description', 'client_id', 'department', 'colour', 'start', 'finish', 'active', 'platform', 'created_by'];

}
