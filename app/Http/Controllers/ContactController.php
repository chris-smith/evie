<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Client;
use App\Project;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Request;

class ContactController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $contacts = Contact::orderby('created_at')->get();

        foreach($contacts as $contact) {
            $client = Client::findOrFail($contact['client_id']);
            $contact['client_id_name'] = $client->name;

            if($contact['project_id']) {
                $project = Project::findOrFail($contact['project_id']);
                $contact['project_id_name'] = $project->name;
            }
        }

        return view('contacts.index', compact('contacts'));

    }

    public function show(Client $client)
    {

        // $client = Client::find($id);

        // dd($client);

        return view ('contacts.show', compact('contact'));

    }


    public function create()
    {

        $clients_raw = Client::orderby('name')->get();

        $clients = array();

        foreach($clients_raw as $client) :

            $clients[$client->id] = $client->name;

        endforeach;


        $projects_raw = Project::orderby('name')->get();

        $projects = array();

        foreach($projects_raw as $project) :

            $projects[$project->id] = $project->name;

        endforeach;


        return view ('contacts.create', compact('clients', 'projects'));

    }


    public function store(HttpRequest $request)
    {

        $this->validate(request(), [

          'name' => 'required',

          'client_id' => 'required'

        ]);

        Contact::create(request()->all());

        return redirect('/contacts');

    }


    public function edit($id)
    {

        $contact = Contact::find($id);

        $clients_raw = Client::orderby('name')->get();

        $clients = array();

        foreach($clients_raw as $client) :

            $clients[$client->id] = $client->name;

        endforeach;


        $projects_raw = Project::orderby('name')->get();

        $projects = array();

        foreach($projects_raw as $project) :

            $projects[$project->id] = $project->name;

        endforeach;


        return view('contacts.edit', compact('contact', 'clients', 'projects'));

    }


    public function update(Request $request, $id)
    {

     // // Client::where('id', $id)->update($request->all());
    // //   return redirect('/clients');

      $contact = Contact::findOrFail($id);
      $input = Request::all();
      $contact->update($input);
      return redirect('/contacts');

    }


    public function destroy($id)
    {

    //   $client = Client::findOrFail($id);
    //   $client->delete();
      //
    //   return redirect('/clients');

    }
}
