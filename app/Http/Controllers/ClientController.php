<?php

namespace App\Http\Controllers;

use App\Client;
use App\User;
use Auth;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Request;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $clients = Client::orderby('created_at')->get();

        foreach($clients as $client) {
            $user = User::findOrFail($client['created_by']);
            $client['created_by_name'] = $user->name;
        }

        // dd($clients);

        return view('clients.index', compact('clients'));

    }


    public function show(Client $client)
    {

        // $client = Client::find($id);

        dd($client);

        return view ('clients.show', compact('client'));

    }


    public function create()
    {

        return view ('clients.create');

    }


    public function store(HttpRequest $request)
    {

        // print_r($request->input('name'));
        $request->offsetSet('created_by', Auth::id());

        $this->validate(request(), [

          'name' => 'required',

          'description' => 'required'

        ]);


        Client::create(request()->all());
        // Client::create(request(['name'), request(['description']), request(['site_url']), active(['active']), 'type', 'colour', 'start', 'finish', 'status', 'framework']));

        return redirect('/clients');

    }


    public function edit($id)//Client $client)
    {
      // $task = Task::findOrFail($id);

      // return view('tasks.edit')->withTask($task);

      $client = Client::find($id);

      return view('clients.edit', compact('client'));

    }


    public function update(Request $request, $id)//Client $client)
    {

      // Client::where('id', $id)->update($request->all());
      // return redirect('/clients');

      $client = Client::findOrFail($id);
      $input = Request::all();
      $client->update($input);
      return redirect('/clients');

    }


    public function destroy($id)
    {

      $client = Client::findOrFail($id);
      $client->delete();

      return redirect('/clients');

    }

}
