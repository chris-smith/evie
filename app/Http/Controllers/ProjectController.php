<?php

namespace App\Http\Controllers;

use App\Client;
use App\User;
use App\Project;
use Auth;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Request;
use Carbon;

class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $projects = Project::orderby('created_at')->get();

        foreach($projects as $project) {
            // $user = User::findOrFail($project['created_by']);
            // $project['created_by_name'] = $user->name;
            $project->format_start = Carbon::createFromFormat('d-m-Y', $project->start)->toFormattedDateString();
            $project->format_finish = Carbon::createFromFormat('d-m-Y', $project->finish)->toFormattedDateString();

            $client = Client::findOrFail($project['client_id']);
            $project['client_id_name'] = $client->name;
        }

        $platforms = array(
            "0" => "WordPress",
            "1" => "Laravel",
            "2" => "Zend",
            "3" => "Yii",
            "4" => "Umbraco",
            "5" => "Mobile Applications"
        );

        $departments = array(
            "0" => "Development",
            "1" => "Marketing"
        );

        return view('projects.index', compact('projects', 'platforms', 'departments'));

    }


    public function show(Project $project)
    {

        // $client = Client::find($id);

        dd($project);

        return view ('clients.show', compact('client'));

    }


    public function create()
    {

        $clients_raw = Client::orderby('name')->get();

        $clients = array();

        foreach($clients_raw as $client) :

            $clients[$client->id] = $client->name;

        endforeach;

        $platforms = array(
            "0" => "WordPress",
            "1" => "Laravel",
            "2" => "Zend",
            "3" => "Yii",
            "4" => "Umbraco",
            "5" => "Mobile Applications"
        );

        $departments = array(
            "0" => "Development",
            "1" => "Marketing"
        );

        return view ('projects.create', compact('clients', 'platforms', 'departments'));

    }


    public function store(HttpRequest $request)
    {

        // print_r($request->input('name'));
        $request->offsetSet('created_by', Auth::id());

        // dd(request()->get('department'));

        $this->validate(request(), [

          'name' => 'required',

          'description' => 'required',

          'start' => 'required',

          'finish' => 'required',

        ]);


        Project::create(request()->all());
        // Client::create(request(['name'), request(['description']), request(['site_url']), active(['active']), 'type', 'colour', 'start', 'finish', 'status', 'framework']));

        return redirect('/projects');

    }


    public function edit($id)
    {
      // $task = Task::findOrFail($id);

      // return view('tasks.edit')->withTask($task);

      $project = Project::find($id);

      $clients_raw = Client::orderby('name')->get();

      $clients = array();

      foreach($clients_raw as $client) :

          $clients[$client->id] = $client->name;

      endforeach;

      $platforms = array(
          "0" => "WordPress",
          "1" => "Laravel",
          "2" => "Zend",
          "3" => "Yii",
          "4" => "Umbraco",
          "5" => "Mobile Applications"
      );

      $departments = array(
          "0" => "Development",
          "1" => "Marketing"
      );

      return view('projects.edit', compact('project', 'clients', 'platforms', 'departments'));

    }


    public function update(Request $request, $id)
    {

      // Client::where('id', $id)->update($request->all());
      // return redirect('/clients');

      $project = Project::findOrFail($id);
      $input = Request::all();
      $project->update($input);
      return redirect('/projects');

    }


    public function destroy($id)
    {

      $project = Project::findOrFail($id);
      $project->delete();

      return redirect('/projects');

    }

}
