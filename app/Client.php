<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $fillable = ['name', 'description', 'site_url', 'active', 'address', 'created_by', 'status', 'framework'];

    // protected $casts = [
    //     'start' => 'date',
    //     'finish' => 'date',
    // ];

}
