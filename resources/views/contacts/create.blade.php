@extends('layouts.index')

@section('content')

    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-10">
                <h1 class="page-header">Create a new contact</h1>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-body">

                        {{ Form::open(['method' => 'POST', 'route' => ['contact.store']]) }}

                            {{ csrf_field() }}

                            <div class="row">

                                <div class="col-xs-3 col-sm-3 col-md-4 col-lg-3">

                                    <div class="form-group">

                                        {{ Form::text('name', '', ['class=form-control', 'placeholder=Name']) }}

                                    </div>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-4 col-lg-3">

                                    <div class="form-group">

                                        {{ Form::text('email', '', ['class=form-control', 'placeholder="Email Address"']) }}

                                    </div>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-4 col-lg-3">

                                    <div class="form-group">

                                        {{ Form::text('phone', '', ['class=form-control', 'placeholder="Phone Number"']) }}

                                    </div>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-4 col-lg-3">

                                    <div class="form-group">

                                        {{ Form::text('skype', '', ['class=form-control', 'placeholder="Skype Username"']) }}

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                    <div class="form-group">

                                        {{ Form::text('role', '', ['class=form-control', 'placeholder="Role"']) }}

                                    </div>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                    <div class="form-group">

                                        {{ Form::select('client_id', $clients, null, ['class=form-control', 'placeholder' => 'Select a client']) }}

                                    </div>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                    <div class="form-group">

                                        {{ Form::select('project_id', $projects, null, ['class=form-control', 'placeholder' => 'Select a project']) }}

                                    </div>

                                </div>

                            </div>

                            <div class='form-group'>

                                <button type="submit" class="btn btn-primary">Add Contact</button>

                            </div>


                            @include('partials.errors')

                        {{ Form::close() }}

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
