<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>evie alpha</title>
<meta name="description" content="Ninja Tune" />
<meta name="keywords" content="Ninja Tune, music, artists, albums, playlists, tracks, songs" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Language" content="en" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
{{-- <link rel="icon" type="image/png" href="/images/favicon.png" /> --}}
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{{-- <script type="text/javascript" src="/bootstrap-slider-master/dist/bootstrap-slider.js"></script> --}}
{{-- <script type="text/javascript" src="/bootstrap-slider-master/dist/bootstrap-slider.min.js"></script> --}}
{{-- <script src="/js/jquery.matchHeight.js"></script> --}}
{{-- <script src="/js/custom.js</script> --}}

{{-- <link rel="stylesheet" href="/bootstrap-slider-master/dist/css/bootstrap-slider.css" type="text/css" /> --}}
{{-- <link rel="stylesheet" href="/bootstrap-slider-master/dist/css/bootstrap-slider.min.css" type="text/css" /> --}}
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="/css/sb-admin-2.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/style.css') }}" rel="stylesheet">

<script src="https://use.fontawesome.com/15e0819775.js"></script>

{{-- <script src="{{ asset('js/sb-admin-2.min.js') }}"></script> --}}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
{{-- <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --}}
<link href="{{ asset('/sbadmin/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">
<link href="{{ asset('/sbadmin/dist/css/sb-admin-2.css') }}" rel="stylesheet">
<link href="{{ asset('/sbadmin//vendor/morrisjs/morris.css')}}" rel="stylesheet">
{{-- <link href="{{ asset('/sbadmin/vendor/font-awesome/css/§-awesome.css') }}" rel="stylesheet" type="text/css"> --}}
<script src="{{ asset('/sbadmin/vendor/metisMenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('/sbadmin/vendor/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('/sbadmin/vendor/morrisjs/morris.min.js') }}"></script>
<script src="{{ asset('/sbadmin/data/morris-data.js') }}"></script>
<script src="{{ asset('/sbadmin/dist/js/sb-admin-2.js') }}"></script>

<link href="{{ asset('css/admin-style.css') }}" rel="stylesheet">

{{-- <script src="https://use.typekit.net/czt6ppb.js"></script> --}}
{{-- <script>try{Typekit.load({ async: true });}catch(e){}</script> --}}

<!-- Bootstrap toggle button -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<!-- Bootstrap date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet">

<!-- Bootstrap colour picker -->
<script src="/boostrap-color-picker/js/bootstrap-colorpicker.min.js"></script>
<link href="/boostrap-color-picker/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<!-- Google Charts -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<?php header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type'); ?>
</head>
<body>
{{ csrf_field() }}
