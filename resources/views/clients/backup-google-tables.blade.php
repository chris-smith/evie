@extends('layouts.index')

@section('content')

    {{-- <div class="container"> --}}

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Welcome {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</h1>
            </div>
        </div>

        {{-- <div class="row">

            <div class="col-lg-12">

                <a class="btn btn-primary btn-lg" href="/client/create" role="button">Add new client</a>

            </div>

        </div> --}}

    	<div class="row">
            <div class="col-lg-12">

                @foreach($clients as $client)
                    <style>
                        #client-{{ $client['id'] }}:hover {
                            background: {{ $client['colour'] }}
                        }
                    </style>
                    <div class="btn btn-sq btn-primary btn-client" id="client-{{ $client['id'] }}"
                    style="-moz-transition: all .3s ease-in;
                    -o-transition: all .3s ease-in;
                    -webkit-transition: all .3s ease-in;
                    transition: all .3s ease-in;">
                        {{-- <i class="fa fa-user fa-5x"></i><br/> --}}
                        <div class="client-container">
                            <div class="client-name">{{ $client['name'] }}</div>
                        </div>
                    </div>

                @endforeach

            </div>

    	</div>

    {{-- </div> --}}

    {{-- <div class="container"> --}}
        @if(count($clients))

            <div class="row">

                <div class="col-lg-12">

                    <script type="text/javascript">
                        google.charts.load("current", {packages:["timeline"]});
                        google.charts.setOnLoadCallback(drawChart);
                        function drawChart() {

                            var container = document.getElementById('clientList');
                            var chart = new google.visualization.Timeline(container);
                            var dataTable = new google.visualization.DataTable();
                            dataTable.addColumn({ type: 'string', id: 'Client' });
                            dataTable.addColumn({ type: 'date', id: 'Start' });
                            dataTable.addColumn({ type: 'date', id: 'End' });
                            dataTable.addRows([
                                @foreach($clients as $client)

                                    @php

                                        $start = strtotime( $client['start'] );
                                        $finish = strtotime( $client['finish'] );
                                        $newstart = date('Y, n, j',$start);
                                        $newfinish = date('Y, n, j', $finish);

                                    @endphp

                                        [ '{{ $client['name'] }}', new Date({{ $newstart }}), new Date({{ $newfinish }}) ],

                                @endforeach
                            ]);

                            var options = {
                                timeline: { singleColor: '#337ab7' },
                                // colors: [
                                //     @foreach($clients as $client)
                                //         '{{ $client['colour'] }}',
                                //     @endforeach
                                //     // '#cbb69d', '#603913', '#c69c6e'
                                // ],
                            };

                            chart.draw(dataTable, options);
                        }
                    </script>

                    <div id="clientList" style="height: 800px;"></div>

                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">

                    <h2>Department Breakdown Concept</h2>
                    <script type="text/javascript">
                        google.charts.load("current", {packages:["timeline"]});
                        google.charts.setOnLoadCallback(drawChart);
                        function drawChart() {

                            var container = document.getElementById('conceptList');
                            var chart = new google.visualization.Timeline(container);
                            var dataTable = new google.visualization.DataTable();
                            dataTable.addColumn({ type: 'string', id: 'Client' });
                            dataTable.addColumn({ type: 'string', id: 'Stage' });
                            dataTable.addColumn({ type: 'date', id: 'Start' });
                            dataTable.addColumn({ type: 'date', id: 'End' });
                            dataTable.addRows([
                                [ 'Green Tomato Cars', 'Design', new Date(2017, 5, 25), new Date(2017, 6, 18) ],
                                [ 'Green Tomato Cars', 'Front-end', new Date(2017, 6, 18), new Date(2017, 7, 3) ],
                                [ 'Green Tomato Cars', 'Back-end', new Date(2017, 7, 3), new Date(2017, 9, 6) ],
                                [ 'Green Tomato Cars', 'Testing', new Date(2017, 9, 6), new Date(2017, 9, 28) ],
                                [ 'Ninja Tune', 'Design', new Date(2017, 3, 12), new Date(2017, 3, 29) ],
                                [ 'Ninja Tune', 'Front-end', new Date(2017, 3, 29), new Date(2017, 4, 26) ],
                                [ 'Ninja Tune', 'Back-end', new Date(2017, 4, 26), new Date(2017, 6, 12) ],
                                [ 'Ninja Tune', 'Testing', new Date(2017, 6, 12), new Date(2017, 7, 27) ],
                                [ 'Free Dating Platform', 'Design', new Date(2017, 7, 8), new Date(2017, 7, 30) ],
                                [ 'Free Dating Platform', 'Front-end', new Date(2017, 7, 30), new Date(2017, 8, 25) ],
                                [ 'Free Dating Platform', 'Back-end', new Date(2017, 8, 25), new Date(2017, 10, 13) ],
                                [ 'Free Dating Platform', 'Testing', new Date(2017, 10, 13), new Date(2017, 11, 6) ],
                            ]);

                            var options = {
                                colors: ['#33b7b2', '#339bb7', '#337ab7', '#3359b7'],
                                timeline: { barLabelStyle: { fontName: 'Garamond', fontSize: 14 } },
                                avoidOverlappingGridLines: false,
                            };

                            chart.draw(dataTable, options);
                        }

                    </script>

                    <div id="conceptList" style="height: 200px;"></div>

                </div>

            </div>

        @endif

    </div>

@endsection
