@extends('layouts.index')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-10">
                <h1 class="page-header">Clients</h1>
            </div>

            <div class="col-lg-2">
                <a class="btn btn-primary btn-lg btn-right" href="/client/create" role="button">Add new client</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <input type="text" class="search form-control" placeholder="Search clients">

                    </div>

                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover results" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th class="sorting_asc">Active</th>
                                    <th>Name</th>
                                    <th>URL</th>
                                    <th>Description</th>
                                    <th>Created By</th>
                                    <th style="width:12%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($clients as $client)

                                    <tr>
                                        <td>{{ $client['active'] }}</td>
                                        <td>{{ $client['name'] }}</td>
                                        <td>{{ $client['site_url'] }}</td>
                                        <td>{{ substr($client['description'], 0, 60) }}@if(strlen($client['description'])>60)... @endif</td>
                                        <td class="center">{{ $client['created_by_name'] }}</td>
                                        <td>
                                            <a href="/clients/{{ $client['id'] }}/edit"><button style="padding:1px 8px!important" type="button" class="btn btn-outline btn-warning">Edit</button></a>
                                            {{ Form::open(['method' => 'DELETE', 'delete-value' => $client['name'], 'class' => 'delete-form', 'route' => ['clients.destroy', $client['id']]]) }}
                                                {{ Form::submit('Delete', ['class' => 'btn btn-outline btn-danger btn-inline-smaller']) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>

                                @endforeach


                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>

        <script type="text/javascript">
            $(document).ready(function() {

                $(".delete-form").submit(function( event ) {
                    if(confirm('Really delete '+$(this).attr('delete-value')+'?')) {
                        //
                    } else
                        event.preventDefault();
                });

                $(".search").keyup(function () {
                    var searchTerm = $(".search").val();
                    console.log(searchTerm);
                    var listItem = $('.results tbody').children('tr');
                    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
                    console.log(searchSplit);

                    $.extend($.expr[':'], {'containsi': function(elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    } });

                    $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e) {
                        $(this).attr('visible','false');
                    });

                    $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e) {
                        $(this).attr('visible','true');
                    });

                    var jobCount = $('.results tbody tr[visible="true"]').length;
                    $('.counter').text(jobCount + ' item');

                    if(jobCount == '0') {$('.no-result').show();}
                    else {$('.no-result').hide();}
                });
            });
        </script>

    </div>

@endsection
