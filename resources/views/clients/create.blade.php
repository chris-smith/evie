@extends('layouts.index')

@section('content')

    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-10">
                <h1 class="page-header">Create a new client</h1>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-body">

                        {{ Form::open(['method' => 'POST', 'route' => ['client.store']]) }}

                            {{ csrf_field() }}

                            <div class="row">

                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">

                                    <div class="form-group">

                                        {{ Form::text('name', '', ['class=form-control', 'placeholder=Name']) }}

                                    </div>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                    <div class="form-group">

                                        {{ Form::text('site_url', '', ['class=form-control', 'placeholder="Site URL"']) }}

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <div class="form-group">

                                        {{Form::textarea('description', '', ['class' => 'form-control', 'rows' => '3', 'placeholder' => 'Description', 'id' => 'description'])}}

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">

                                    <div class="form-group">

                                        {{Form::textarea('address', '', ['class' => 'form-control', 'rows' => '3', 'placeholder' => 'Street Address', 'id' => 'address'])}}

                                    </div>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">

                                    <div class="form-group">

                                        {{ Form::checkbox('active', '1', true, ['disabled=disabled', 'data-toggle=toggle', 'data-on=Active', 'data-off=Inactive', 'data-onstyle=success', 'data-offstyle=danger']) }}

                                    </div>

                                </div>

                            </div>

                            <div class='form-group'>

                              <button type="submit" class="btn btn-primary">Add Client</button>

                            </div>

                            @include('partials.errors')

                        {{ Form::close() }}

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
