
@extends('layouts.index')

@section('content')

    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-10">
                <h1 class="page-header">Edit project: {{ $project->name }}</h1>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">

                <div class="panel panel-default">

                        <div class="panel-body">

                            {{ Form::open(['method' => 'PATCH', 'route' => ['projects.update', $project->id]]) }}

                            {{ csrf_field() }}

                            @include('projects.partials.timeline')

                            @include('projects.partials.edit-details')

                            @include('partials.errors')

                            @include('projects.partials.project-snapshot')

                            <div class="panel-group" id="accordion">

                                @include('projects.partials.phase-design')

                                @include('projects.partials.phase-front')

                                @include('projects.partials.phase-back')

                                @include('projects.partials.phase-testing')

                            </div>

                            <script type="text/javascript" src="{{ URL::asset('js/project-phases.js') }}"></script>

                        {{ Form::close() }}

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
