<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="column-bottom">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="row inner-header">

                        <div class="col-lg-12">

                            <h3>Status Snapshot</h3>

                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="snapshot-title">Design:</div>

                    <div class="progress">

                        <div id="design-snapshot" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            0%
                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="snapshot-title">Front-End:</div>

                    <div class="progress">

                        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="32" aria-valuemin="0" aria-valuemax="100" style="width: 32%;">
                            32%
                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="snapshot-title">Back-End:</div>

                    <div class="progress">

                        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="19" aria-valuemin="0" aria-valuemax="100" style="width: 19%;">
                            19%
                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="snapshot-title">Testing:</div>

                    <div class="progress">

                        <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="11" aria-valuemin="0" aria-valuemax="100" style="width: 11%;">
                            11%
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
