<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <script type="text/javascript">
            google.charts.load("current", {packages:["timeline"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {

                var container = document.getElementById('timingChart');
                var chart = new google.visualization.Timeline(container);
                var dataTable = new google.visualization.DataTable();
                dataTable.addColumn({ type: 'string', id: 'Client' });
                dataTable.addColumn({ type: 'string', id: 'Stage' });
                dataTable.addColumn({ type: 'date', id: 'Start' });
                dataTable.addColumn({ type: 'date', id: 'End' });
                dataTable.addRows([
                    // [ 'Green Tomato Cars', 'Design', new Date(2017, 5, 25), new Date(2017, 6, 18) ],
                    // [ 'Green Tomato Cars', 'Front-end', new Date(2017, 6, 18), new Date(2017, 7, 3) ],
                    // [ 'Green Tomato Cars', 'Back-end', new Date(2017, 7, 3), new Date(2017, 9, 6) ],
                    // [ 'Green Tomato Cars', 'Testing', new Date(2017, 9, 6), new Date(2017, 9, 28) ],
                    [ 'Ninja Tune', 'Design', new Date(2017, 3, 12), new Date(2017, 3, 29) ],
                    [ 'Ninja Tune', 'Front-end', new Date(2017, 3, 29), new Date(2017, 4, 26) ],
                    [ 'Ninja Tune', 'Back-end', new Date(2017, 4, 26), new Date(2017, 6, 12) ],
                    [ 'Ninja Tune', 'Testing', new Date(2017, 6, 12), new Date(2017, 7, 27) ],
                    // [ 'Free Dating Platform', 'Design', new Date(2017, 7, 8), new Date(2017, 7, 30) ],
                    // [ 'Free Dating Platform', 'Front-end', new Date(2017, 7, 30), new Date(2017, 8, 25) ],
                    // [ 'Free Dating Platform', 'Back-end', new Date(2017, 8, 25), new Date(2017, 10, 13) ],
                    // [ 'Free Dating Platform', 'Testing', new Date(2017, 10, 13), new Date(2017, 11, 6) ],
                ]);

                var options = {
                    colors: ['#F15E8C', '#EC1A5C', '#a51240', '#5e0a24'],
                    timeline: { barLabelStyle: { fontName: 'Garamond', fontSize: 14 } },
                    avoidOverlappingGridLines: false,
                    backgroundColor: '#2d2d2d'
                };

                chart.draw(dataTable, options);
            }

        </script>

        <div id="timingChart"></div>

    </div>

</div>
