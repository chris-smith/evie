<div class="panel panel-default">

    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">

        <div class="panel-heading">

            <h4 class="panel-title">

                Design Phase

            </h4>

        </div>

    </a>

    <div id="collapse1" class="panel-collapse collapse">

        <div class="panel-body">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="column-bottom">

                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                <div class="progress phase-progress">

                                    <div id="progress-bar-design" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                        0%
                                    </div>

                                </div>

                            </div>

                        </div>

                        @php

                            $design_1 = array('name'=>'Homepage', 'design'=>1, 'lead'=>1, 'front'=>0, 'seo'=>0, 'pm'=>0, 'back'=>0, 'am'=>0, 'client'=>0);
                            // $design_2 = array('name'=>'About us', 'design'=>1, 'lead'=>1, 'front'=>1, 'back'=>1, 'client'=>1);
                            // $design_3 = array('name'=>'Contact', 'design'=>1, 'lead'=>1, 'front'=>1, 'back'=>1, 'client'=>0);
                            // $design_4 = array('name'=>'Services', 'design'=>1, 'lead'=>0, 'front'=>0, 'back'=>0, 'client'=>0);
                            // $design_5 = array('name'=>'Careers', 'design'=>1, 'lead'=>0, 'front'=>0, 'back'=>0, 'client'=>0);
                            // $design_6 = array('name'=>'Sectors', 'design'=>0, 'lead'=>0, 'front'=>0, 'back'=>0, 'client'=>0);
                            // $design_7 = array('name'=>'Team', 'design'=>0, 'lead'=>0, 'front'=>0, 'back'=>0, 'client'=>0);
                            // $design_8 = array('name'=>'Landing', 'design'=>0, 'lead'=>0, 'front'=>0, 'back'=>0, 'client'=>0);

                            $designs = array($design_1/*,$design_2,$design_3,$design_4,$design_5,$design_6,$design_7,$design_8*/);

                        @endphp

                        <div class="control-container">

                            <div id="add-row" onclick="addRow()" class="btn btn-primary">Add Row</div>

                            <table class="table" id="myTable">
                                <thead>
                                    <tr>
                                        <th width="30%" class="text-center">Page Name</th>
                                        <th class="text-center">Design</th>
                                        <th class="text-center">Lead</th>
                                        <th class="text-center">Front</th>
                                        <th class="text-center">SEO</th>
                                        <th class="text-center">PM</th>
                                        <th class="text-center">Back</th>
                                        <th class="text-center">AM</th>
                                        <th class="text-center">Client</th>
                                        <th class="text-center">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($designs as $index => $design)

                                        <tr class="text-center" id="row-{{ $index }}">

                                            @include('projects.partials.design-part')

                                        </tr>

                                    @endforeach

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
