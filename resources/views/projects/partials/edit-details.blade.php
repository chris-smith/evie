<div class="row">

    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">

        <div class="column-left">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="row inner-header">

                        <div class="col-lg-12">

                            <h3>Details</h3>

                        </div>

                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group">

                        {{ Form::label('name', 'Title') }}

                        {{ Form::text('name', $project->name, ['class=form-control', 'placeholder=Name', 'id=name']) }}
                        <script type="text/javascript">
                            $(function() {
                                $('#name').on('input', function() {
                                    $('.page-header').text('Edit project: '+$('#name').val());
                                });
                            });
                        </script>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group">

                        {{ Form::label('name', 'Description') }}

                        {{ Form::textarea('description', $project->description, ['class=form-control', 'rows=6', 'placeholder="Description"']) }}

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group" id="daterange-container">

                        {{ Form::label('start', 'Start & End Dates') }}

                        <div class="input-daterange input-group" id="datepicker">

                            {{ Form::text('start', $project->start, ['class="input-sm form-control"', 'id=start', 'placeholder="Start"']) }}

                            <span class="input-group-addon">to</span>

                            {{ Form::text('finish', $project->start, ['class="input-sm form-control"', 'id=finish', 'placeholder="Projected Finish"']) }}

                        </div>

                        <script>
                            $('#daterange-container .input-daterange').datepicker({
                                weekStart: 1,
                                format: "dd-mm-yyyy",
                                daysOfWeekDisabled: "6,0",
                                daysOfWeekHighlighted: "1,2,3,4,5",
                                todayHighlight: true
                            });
                        </script>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class='form-group'>

                        <button type="submit" class="btn btn-primary">Save Changes</button>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

        <div class="column-right">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="row inner-header">

                        <div class="col-lg-12">

                            <h3>Settings</h3>

                        </div>

                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group">

                        {{ Form::label('client_id', 'Client') }}

                        {{ Form::select('client_id', $clients, $project->client_id, ['class=form-control', 'required', 'placeholder' => 'Select a related client']) }}

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group">

                        {{ Form::label('active', 'Status') }}

                        {{ Form::select('active', array(1=>'Active',0=>'Inactive'), $project->active, ['class=form-control', 'required', 'placeholder' => 'Project Status']) }}

                        {{-- {{ Form::checkbox('active', '1', true, ['class=form-control', 'disabled=disabled', 'data-toggle=toggle', 'data-on=Active', 'data-off=Complete', 'data-onstyle=success', 'data-offstyle=danger']) }} --}}

                    </div>

                </div>

                {{-- <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                    <div class="form-group">

                        <a href="#" class="btn btn-default" style="border-color:{{ $project->colour }}" id="cp4">Change project colour</a>
                        <input type="hidden" value="#FFFFFF" class="form-control" name="colour" id="colour" />
                        <script type="text/javascript">
                            $(function() {
                                $('#cp4').colorpicker().on('changeColor', function(e) {
                                    $('#colour').val(e.color);
                                    $('#cp4')[0].style.borderColor = e.color.toString('rgba');

                                });
                            });
                        </script>

                    </div>

                </div> --}}

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group">

                        {{ Form::label('department', 'Department') }}

                        {{ Form::select('department', $departments, $departments[$project->department], ['class=form-control', 'id=department', 'placeholder' => 'Department']) }}

                        <script type="text/javascript">
                            $(function() {
                                $('#department').on('change', function() {
                                    var dept = $('#department').find(":selected").text();
                                    if(dept=='Marketing')
                                        $('.platform-row').hide();
                                    else
                                        $('.platform-row').show();
                                });
                            });
                        </script>

                    </div>

                </div>

            </div>

            <div class="row platform-row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group">

                        {{ Form::label('platform', 'Platform') }}

                        {{ Form::select('platform', $platforms, $platforms[$project->platform], ['class=form-control', 'placeholder' => 'Project Platform']) }}

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
