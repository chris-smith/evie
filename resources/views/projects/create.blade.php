@extends('layouts.index')

@section('content')

    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-10">
                <h1 class="page-header">Create a new project</h1>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-body">

                        {{ Form::open(['method' => 'POST', 'route' => ['project.store']]) }}

                            {{ csrf_field() }}

                            <div class="row">

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                    <div class="form-group">

                                        {{ Form::text('name', '', ['class=form-control', 'placeholder=Name']) }}

                                    </div>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                    <div class="form-group">

                                        {{ Form::select('client_id', $clients, null, ['class=form-control', 'required', 'placeholder' => 'Select a related client']) }}

                                    </div>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">

                                    <div class="form-group">

                                        <a href="#" class="btn btn-default" id="cp4">Set project colour</a>
                                        <input type="hidden" value="#FFFFFF" class="form-control" name="colour" id="colour" />
                                        <script type="text/javascript">
                                            $(function() {
                                                $('#cp4').colorpicker().on('changeColor', function(e) {
                                                    $('#colour').val(e.color);
                                                    $('#cp4')[0].style.borderColor = e.color.toString('rgba');
                                                });
                                            });
                                        </script>

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <div class="form-group">

                                        {{ Form::textarea('description', '', ['class=form-control', 'rows=6', 'placeholder="Description"']) }}

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                    <div class="form-group" id="daterange-container">

                                        <div class="input-daterange input-group" id="datepicker">

                                            {{ Form::text('start', '', ['class="input-sm form-control"', 'id=start', 'placeholder="Start"']) }}

                                            <span class="input-group-addon">to</span>

                                            {{ Form::text('finish', '', ['class="input-sm form-control"', 'id=finish', 'placeholder="Projected Finish"']) }}

                                        </div>

                                        <script>
                                            $('#daterange-container .input-daterange').datepicker({
                                                weekStart: 1,
                                                format: "dd-mm-yyyy",
                                                daysOfWeekDisabled: "6,0",
                                                daysOfWeekHighlighted: "1,2,3,4,5",
                                                todayHighlight: true
                                            });
                                        </script>

                                    </div>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                                    <div class="form-group">

                                        {{ Form::select('department', $departments, null, ['class=form-control', 'placeholder' => 'Department']) }}

                                    </div>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                                    <div class="form-group">

                                        {{ Form::select('platform', $platforms, null, ['class=form-control', 'placeholder' => 'Project Platform']) }}

                                    </div>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">

                                    <div class="form-group">

                                        {{ Form::checkbox('active', '1', true, ['disabled=disabled', 'data-toggle=toggle', 'data-on=Active', 'data-off=Complete', 'data-onstyle=success', 'data-offstyle=danger']) }}

                                    </div>

                                </div>

                            </div>

                            <div class='form-group'>

                                <button type="submit" class="btn btn-primary">Add project</button>

                            </div>


                            @include('partials.errors')

                        {{ Form::close() }}

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
