@extends('layouts.index')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-10">
                <h1 class="page-header">Projects</h1>
            </div>

            <div class="col-lg-2">
                <a class="btn btn-primary btn-lg btn-right" href="/project/create" role="button">Add new project</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <input type="text" class="search form-control" placeholder="Search projects">

                    </div>

                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover results" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th class="sorting_asc">Name</th>
                                    <th>Description</th>
                                    <th>Client</th>
                                    <th>Department</th>
                                    <th>Platform</th>
                                    <th>Timeline</th>
                                    <th style="width:10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($projects as $project)

                                    <tr>
                                        <td>{{ $project['name'] }}</td>
                                        <td>{{ substr($project['description'], 0, 60) }}@if(strlen($project['description'])>60)... @endif</td>
                                        <td>{{ $project['client_id_name'] }}</td>
                                        <td>{{ $departments[$project['department']] }}</td>
                                        <td>{{ $platforms[$project['platform']] }}</td>
                                        <td>{{ $project['format_start'] }} - {{ $project['format_finish'] }}</td>
                                        <td>
                                            <a href="/projects/{{ $project['id'] }}/edit"><button style="padding:1px 8px!important" type="button" class="btn btn-outline btn-warning">Edit</button></a>
                                            <button style="padding:1px 8px!important" type="button" class="btn btn-outline btn-danger">Delete</button>
                                        </td>
                                    </tr>

                                @endforeach


                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>

        <script type="text/javascript">
            $(document).ready(function() {

                $(".search").keyup(function () {
                    var searchTerm = $(".search").val();
                    console.log(searchTerm);
                    var listItem = $('.results tbody').children('tr');
                    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
                    console.log(searchSplit);

                    $.extend($.expr[':'], {'containsi': function(elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    } });

                    $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e) {
                        $(this).attr('visible','false');
                    });

                    $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e) {
                        $(this).attr('visible','true');
                    });

                    var jobCount = $('.results tbody tr[visible="true"]').length;
                    $('.counter').text(jobCount + ' item');

                    if(jobCount == '0') {$('.no-result').show();}
                    else {$('.no-result').hide();}
                });
            });
        </script>

    </div>

@endsection
