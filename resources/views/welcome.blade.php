<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #a81b19;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links a:hover {
              color: #c25f5e;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .blog-title {
              margin-bottom: 0;
              font-size: 80px;
              font-weight: 300;
              font-family: "Helvetica Neue", 'Work Sans', Helvetica, Arial, sans-serif;
              color: transparent;
            }

            .reverse {
              -moz-transform: scale(-1, 1);
              -webkit-transform: scale(-1, 1);
              -o-transform: scale(-1, 1);
              -ms-transform: scale(-1, 1);
              transform: scale(-1, 1)!important;
            }

            .flasher {
              /*-webkit-animation: color-change 16s infinite;
              -moz-animation: color-change 16s infinite;
              -o-animation: color-change 16s infinite;
              -ms-animation: color-change 16s infinite;
              animation: color-change 16s infinite;*/

              /*font-family: RealHelvetica, "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;*/

              font-family: "Helvetica Neue", 'Work Sans', Helvetica, Arial, sans-serif;

              /*font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;*/

              background: -webkit-linear-gradient(225deg, #a81b19, #f6e8e8, #a81b19);
              -webkit-background-clip: text;
              -webkit-text-fill-color: transparent;
              background-size: 800px 800px;
              animation: bgColour 12s ease infinite;
            }


            @keyframes bgColour {
              0% {
                background-position: 0% 100%
              }
              50% {
                background-position: 100% 0%
              }
              100% {
                background-position: 100% 0%
              }
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/clients') }}">Clients</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                  <h1 id="site-title" class="blog-title"><span class="flasher">evie</span></h1>
                  {{-- <span class="reverse">e</span> --}}
                </div>


                {{-- <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div> --}}
            </div>
        </div>
    </body>
</html>
