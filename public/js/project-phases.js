$(document).ready(function() {
    updateBarProgress($('#progress-bar-design'), $('#design-snapshot'));
});

function addRow(input) {
    var table = document.getElementById("myTable");
    var line = $('#myTable tbody tr').length;
    var row = table.insertRow(table.rows.length);
    row.className="text-center";
    row.id="row-"+line;
    var cell = row.insertCell(0);
    cell.style.width = "30%";
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="page-name" type="text" />'+
                    '</div>';
    cell = row.insertCell(1);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="design" type="checkbox" value="1">'+
                    '</div>';
    cell = row.insertCell(2);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="design-lead" type="checkbox" value="1">'+
                    '</div>';
    cell = row.insertCell(3);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="front-dev" type="checkbox" value="1">'+
                    '</div>';
    cell = row.insertCell(4);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="seo" type="checkbox" value="1">'+
                    '</div>';
    cell = row.insertCell(5);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="pm" type="checkbox" value="1">'+
                    '</div>';
    cell = row.insertCell(6);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="back-dev" type="checkbox" value="1">'+
                    '</div>';
    cell = row.insertCell(7);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="am" type="checkbox" value="1">'+
                    '</div>';
    cell = row.insertCell(8);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        '<input name="client" type="checkbox" value="1">'+
                    '</div>';
    cell = row.insertCell(9);
    cell.className="text-center";
    cell.innerHTML = '<div class="">'+
                        'X'+
                    '</div>';
}

function updateBarProgress(bar, snapshot) {
    var count = 0;
    var total = 0;
    $('#myTable').find('input[type=checkbox]').each(function(i, v) {
        total++;
        if($(v).is(':checked'))
            count++;
    });

    var perc = Math.round((count/total)*100);

    bar.attr('aria-valuenow', perc);
    bar.css('width', perc+'%');
    bar.text(perc+'%');
    if(perc>=100) {
        bar.css('background-color', '#5cb85c')
    } else {
        bar.css('background-color', '#F15E8C')
    }

    snapshot.attr('aria-valuenow', perc);
    snapshot.css('width', perc+'%');
    snapshot.text(perc+'%');
    if(perc>=100) {
        snapshot.css('background-color', '#5cb85c')
    } else {
        snapshot.css('background-color', '#F15E8C')
    }
}

$('#myTable').on('change', 'input', function () {
    updateBarProgress($('#progress-bar-design'), $('#design-snapshot'));
});

$('.delete-row').click(function() {
    console.log($(this).attr('id'));
    var id = $(this).attr('count');
    $('#row-'+id).fadeOut();
});
