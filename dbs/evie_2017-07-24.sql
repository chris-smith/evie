# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Database: evie
# Generation Time: 2017-07-24 17:06:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `colour` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finish` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `framework` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;

INSERT INTO `clients` (`id`, `name`, `description`, `type`, `colour`, `start`, `finish`, `status`, `framework`, `created_at`, `updated_at`)
VALUES
	(1,'Green Tomato Cars','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#00bb1e','05-06-2017','18-09-2017','on','Wordpress','2017-07-22 19:35:22','2017-07-22 19:35:22'),
	(3,'Death Penalty Project','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#d3d3d3','15-07-2017','28-09-2017','on','Wordpress','2017-07-23 10:36:43','2017-07-23 10:36:43'),
	(4,'Free Dating Platform','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',2,'#f500dd','10-07-2017','22-12-2017','on','Yii','2017-07-23 10:37:33','2017-07-23 10:37:33'),
	(5,'Plan It','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#00aabb','01-04-2017','31-08-2017','on','Wordpress','2017-07-23 10:38:06','2017-07-23 10:38:06'),
	(6,'Wisteria','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#7f00bb','11-05-2017','29-07-2017','on','Wordpress','2017-07-23 10:39:09','2017-07-23 10:39:09'),
	(7,'Ninja Tune','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#eff500','10-03-2017','30-09-2017','on','Laravel','2017-07-23 10:40:10','2017-07-23 10:40:10'),
	(8,'Milk & More','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#8de1ed','22-05-2017','16-08-2017','on','Other','2017-07-23 10:40:44','2017-07-23 10:40:44'),
	(9,'KK Designs','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#ffe8a4','08-07-2017','22-11-2017','on','Wordpress','2017-07-23 10:41:27','2017-07-23 10:41:27'),
	(10,'Health Online','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#5ade5e','26-08-2017','13-01-2018','on','Laravel','2017-07-23 10:42:08','2017-07-23 10:42:08'),
	(11,'Synthetron','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#adbce6','06-07-2017','25-08-2017','on','Wordpress','2017-07-23 10:42:41','2017-07-23 10:42:41'),
	(12,'ReAD','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#ff4444','11-08-2017','25-11-2017','on','Wordpress','2017-07-23 10:43:09','2017-07-23 10:43:09'),
	(13,'Mercia','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#c8f777','23-09-2017','29-12-2017','on','ASP','2017-07-23 10:43:39','2017-07-23 10:43:39'),
	(14,'SWAT','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#d47e03','16-11-2017','21-12-2017','on','ASP','2017-07-23 10:44:21','2017-07-23 10:44:21'),
	(15,'Inokim','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#636363','09-06-2017','29-07-2017','on','Wordpress','2017-07-23 10:45:11','2017-07-23 10:45:11'),
	(16,'Skytrax Ratings','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#3ee3da','01-03-2017','24-06-2017','on','Wordpress','2017-07-23 10:45:44','2017-07-23 10:45:44'),
	(17,'Discovery App','Etiam nulla dolor, interdum et nibh eu, commodo rhoncus nulla. Sed pretium nisl at libero bibendum, in efficitur risus pellentesque. Integer maximus orci ac ante tincidunt, eu mollis augue feugiat. Aliquam erat volutpat. Aliquam erat volutpat. Aliquam varius sagittis diam nec imperdiet. Etiam erat elit, venenatis vitae fringilla vitae, ultrices a nunc. Morbi lacus diam, lobortis eu vestibulum quis, rhoncus ut odio.',1,'#ff8383','04-08-2017','28-12-2017','on','Other','2017-07-23 10:48:57','2017-07-23 10:48:57');

/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(16,'2014_10_12_000000_create_users_table',1),
	(17,'2014_10_12_100000_create_password_resets_table',1),
	(18,'2017_07_22_000451_create_clients_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Chris','chris.smith@minttwist.com','$2y$10$Usj1ONV/Z1neEYmrwSkapOzh2/Dv4aGpMwX.RYZLZ6IZoP1g.oAP2',NULL,'2017-07-24 13:57:12','2017-07-24 13:57:12');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
