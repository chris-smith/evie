<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index');

Route::get('/clients', 'ClientController@index');
Route::post('/clients', array('as'=>'client.store', 'uses'=>'ClientController@store'));
Route::get('/clients/{client}', 'ClientController@show');
Route::get('/client/create', 'ClientController@create');
Route::get('clients/{id}/edit', 'ClientController@edit');
Route::match(array('PUT', 'PATCH'), "/clients/{id}", array('as'=>'clients.update', 'uses'=>'ClientController@update'));
Route::delete('clients/{id}', array('as'=>'clients.destroy', 'uses'=>'ClientController@destroy'));

Route::get('/contacts', 'ContactController@index');
Route::post('/contacts', array('as'=>'contact.store', 'uses'=>'ContactController@store'));
Route::get('/contacts/{contact}', 'ContactController@show');
Route::get('/contact/create', 'ContactController@create');
Route::get('/contacts/{id}/edit', 'ContactController@edit');
Route::match(array('PUT', 'PATCH'), "/contacts/{id}", array('as'=>'contacts.update', 'uses'=>'ContactController@update'));
Route::delete('contacts/{id}', array('as'=>'contacts.destroy', 'uses'=>'ContactController@destroy'));

Route::get('/projects', 'ProjectController@index');
Route::post('/projects', array('as'=>'project.store', 'uses'=>'ProjectController@store'));
Route::get('/projects/{project}', 'ProjectController@show');
Route::get('/project/create', 'ProjectController@create');
Route::get('/projects/{id}/edit', 'ProjectController@edit');
Route::match(array('PUT', 'PATCH'), "/projects/{id}", array('as'=>'projects.update', 'uses'=>'ProjectController@update'));
Route::delete('projects/{id}', array('as'=>'projects.destroy', 'uses'=>'ProjectController@destroy'));
